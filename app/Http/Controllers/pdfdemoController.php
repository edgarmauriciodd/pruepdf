<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OPDF;
use setasign\Fpdi\Tcpdf\Fpdi;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class pdfdemoController extends Controller
{
    public function pdfd()
    {
        //dd('ksdm');
        //funcion para generar nombre aleatorio unico
        function generateRandomString($length = 10)
        {
            $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString     = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
        //variable de nombre aleatorio generado
        $nombrepdf = generateRandomString();
        //ruta donde se guardará el archivo
        $path = public_path($nombrepdf . '.pdf');

        //certificados
        $certificate = 'file://' . realpath('tcpdf.crt');
        $info        = array(
            'Name'        => 'TCPDF',
            'Location'    => 'Office',
            'Reason'      => 'Testing TCPDF',
            'ContactInfo' => 'http://www.tcpdf.org',
        );

        //CODIGO QR: se obtiene un codigo qr y se decodifica en base64
        $contenidoImagen = (QrCode::format('png')->size(100)->generate('HOLA JEAN'));
        $imagenBase64    = base64_encode($contenidoImagen);

        //generando pdf con DomPDF
        $pdf = \OPDF::loadView('/pdf/077', compact('imagenBase64', 'var2'));
        //guardando pdf en ruta path
        $pdf->save($path . '');
        $pdf->download($nombrepdf . '.pdf');

        //generando pdf con FPDI extends TCPDF
        $pdf2 = new FPDI();
        // $pdf2->AddPage('P', 'A4');
        // $pdf2->setSourceFile($path);
        // $tplId = $pdf2 -> importPage([1,2,3]);
        // $pdf2 -> useTemplate($tplId, null,null,null,210,true);

        $pageCount = $pdf2->setSourceFile($path);
        // iterate through all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            // import a page
            $pdf2->AddPage('P', 'A4');
            $templateId = $pdf2->importPage($pageNo);
            // use the imported page
            $pdf2->useTemplate($templateId);
            // $pdf2->SetXY(5, 5);
        }

        //BORRANDO ARCHIVO PATH
        unlink($path);
//unlink($path.'l');
        // try {
        //     unlink($path).'l';
        // } catch(Exception $e) {
        //     print "error!";
        //     print $e;
        // }

        //FIRMANDO DOCUMENTO
        $pdf2->setSignature($certificate, $certificate, 'tcpdfdemo', '', 2, $info);
        $pdf2->Output("reporte.pdf", "I");
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
